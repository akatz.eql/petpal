package fr.eql.aicap1.petpal.business.impl;

import fr.eql.aicap1.petpal.business.LoginBusiness;
import fr.eql.aicap1.petpal.dao.OwnerDao;
import fr.eql.aicap1.petpal.entity.Owner;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(LoginBusiness.class)
@Stateless
public class LoginBusinessImpl implements LoginBusiness {

    @EJB
    OwnerDao ownerDao;

    @Override
    public Owner authenticate(String login, String password) {
        return ownerDao.authenticate(login, password);
    }
}
