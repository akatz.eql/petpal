package fr.eql.aicap1.petpal.business.impl;

import fr.eql.aicap1.petpal.business.SpaceBusiness;
import fr.eql.aicap1.petpal.dao.CatDao;
import fr.eql.aicap1.petpal.dao.PetCategoryDao;
import fr.eql.aicap1.petpal.entity.Cat;
import fr.eql.aicap1.petpal.entity.Owner;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(SpaceBusiness.class)
@Stateless
public class SpaceBusinessImpl implements SpaceBusiness {

    @EJB
    CatDao catDao;

    @EJB
    PetCategoryDao petCategoryDao;

    @Override
    public Owner getOwnerUpdatedWithPets(Owner owner) {
        owner.getPets().clear();
        owner.getPets().addAll(catDao.findByOwner(owner));
        return owner;
    }

    @Override
    public Owner getOwnerUpdatedWithFavoritePetCategories(Owner owner) {
        owner.getFavoritePetCategories().addAll(petCategoryDao.findByOwner(owner));
        return owner;
    }

    @Override
    public void insertCat(Cat cat, Owner owner) {
        catDao.insertCat(cat, owner);
    }
}
