package fr.eql.aicap1.petpal.business.impl;

import fr.eql.aicap1.petpal.business.GalleryBusiness;
import fr.eql.aicap1.petpal.dao.CatDao;
import fr.eql.aicap1.petpal.dao.OwnerDao;
import fr.eql.aicap1.petpal.entity.Cat;
import fr.eql.aicap1.petpal.entity.Owner;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

@Remote(GalleryBusiness.class)
@Stateless
public class GalleryBusinessImpl implements GalleryBusiness {

    @EJB
    private OwnerDao ownerDao;

    @EJB
    private CatDao catDao;

    @Override
    public Owner getOwnerUpdatedWithPets(Owner owner) {
        List<Cat> ownerCats = catDao.findByOwner(owner);
        owner.getPets().addAll(ownerCats);
        return owner;
    }

    @Override
    public List<Owner> findAllOwnersButSelf(Owner self) {
        return ownerDao.findAllButSelf(self);
    }
}
