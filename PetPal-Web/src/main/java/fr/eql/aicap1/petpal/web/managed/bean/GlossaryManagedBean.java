package fr.eql.aicap1.petpal.web.managed.bean;

import fr.eql.aicap1.petpal.business.GlossaryBusiness;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "mbGlossary")
@ViewScoped
public class GlossaryManagedBean {

    private List<String> expressions;
    private String selectedExpression = "";

    @EJB
    GlossaryBusiness glossaryBusiness;

    @PostConstruct
    public void init() {
        expressions = glossaryBusiness.findGlossary();
        if (!expressions.isEmpty()) {
            selectedExpression = expressions.get(0);
        }
    }

    public String fetchExtract() {
        return glossaryBusiness.fetchExtract(selectedExpression);
    }

    public void updateSelectedExpression(String expression) {
        selectedExpression = expression;
    }

    /// Getters ///
    public List<String> getExpressions() {
        return expressions;
    }
    public String getSelectedExpression() {
        return selectedExpression;
    }

    /// Setters ///
    public void setSelectedExpression(String selectedExpression) {
        this.selectedExpression = selectedExpression;
    }
}
