package fr.eql.aicap1.petpal.web.managed.bean;

import fr.eql.aicap1.petpal.business.GalleryBusiness;
import fr.eql.aicap1.petpal.entity.Cat;
import fr.eql.aicap1.petpal.entity.Owner;
import fr.eql.aicap1.petpal.web.util.DateUtils;
import fr.eql.aicap1.petpal.web.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@ManagedBean(name = "mbGallery")
@ViewScoped
public class GalleryManagedBean {

    private Owner connectedOwner;
    private List<Owner> owners;
    private Owner selectedOwner;
    private Cat selectedCat;

    @EJB
    private GalleryBusiness galleryBusiness;

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        connectedOwner = (Owner) session.getAttribute("connectedOwner");
        owners = findAllOwnersButSelf(connectedOwner);
    }

    public List<Owner> findAllOwnersButSelf(Owner self) {
        return galleryBusiness.findAllOwnersButSelf(self);
    }

    public void resetSelectedCat() {
        selectedCat = null;
    }

    public Owner getOwnerUpdatedWithPets(Owner owner) {
        if (owner != null) {
            return galleryBusiness.getOwnerUpdatedWithPets(owner);
        }
        return null;
    }

    public String caseCorrectedSelectedCatBreed() {
        return StringUtils.firstLetterCapitalized(selectedCat.getBreed().toString());
    }

    public String selectedCatFullBirthDate() {
        return DateUtils.fullDate(selectedCat.getBirthdate());
    }

    /// Getters ///
    public List<Owner> getOwners() {
        return owners;
    }
    public Owner getSelectedOwner() {
        return selectedOwner;
    }
    public Cat getSelectedCat() {
        return selectedCat;
    }

    /// Setters ///
    public void setSelectedOwner(Owner selectedOwner) {
        this.selectedOwner = selectedOwner;
    }
    public void setSelectedCat(Cat selectedCat) {
        this.selectedCat = selectedCat;
    }
}
