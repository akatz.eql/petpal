package fr.eql.aicap1.petpal.business;

import fr.eql.aicap1.petpal.entity.Cat;
import fr.eql.aicap1.petpal.entity.Owner;

public interface SpaceBusiness {

    Owner getOwnerUpdatedWithPets(Owner owner);
    Owner getOwnerUpdatedWithFavoritePetCategories(Owner owner);
    void insertCat(Cat cat, Owner owner);
}
