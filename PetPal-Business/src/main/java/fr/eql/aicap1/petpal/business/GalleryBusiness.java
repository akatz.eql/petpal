package fr.eql.aicap1.petpal.business;

import fr.eql.aicap1.petpal.entity.Owner;

import java.util.List;

public interface GalleryBusiness {

    Owner getOwnerUpdatedWithPets(Owner owner);
    List<Owner> findAllOwnersButSelf(Owner self);
}
