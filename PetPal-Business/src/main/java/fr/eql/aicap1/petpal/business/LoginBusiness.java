package fr.eql.aicap1.petpal.business;

import fr.eql.aicap1.petpal.entity.Owner;

public interface LoginBusiness {

    Owner authenticate(String login, String password);
}
