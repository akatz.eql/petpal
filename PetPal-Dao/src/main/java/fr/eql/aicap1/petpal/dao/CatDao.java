package fr.eql.aicap1.petpal.dao;

import fr.eql.aicap1.petpal.entity.Cat;
import fr.eql.aicap1.petpal.entity.Owner;

import java.util.List;

public interface CatDao {

    void insertCat(Cat cat, Owner owner);
    boolean exists(Cat cat);
    int countByOwner(Owner owner);
    List<Cat> findByOwner(Owner owner);
}
