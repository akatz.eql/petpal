package fr.eql.aicap1.petpal.dao;

import java.util.List;

public interface GlossaryDao {

    List<String> findAll();
}
