package fr.eql.aicap1.petpal.dao;

import fr.eql.aicap1.petpal.entity.Owner;

import java.util.List;

public interface OwnerDao {

    Owner authenticate(String login, String password);
    List<Owner> findAllButSelf(Owner self);
    boolean insertOwner(Owner owner);
    boolean exists(Owner owner);
}
