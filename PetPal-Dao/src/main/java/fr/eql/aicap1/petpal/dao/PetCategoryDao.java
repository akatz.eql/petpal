package fr.eql.aicap1.petpal.dao;

import fr.eql.aicap1.petpal.entity.Owner;
import fr.eql.aicap1.petpal.entity.PetCategory;

import java.util.List;

public interface PetCategoryDao {

    List<PetCategory> findByOwner(Owner owner);
}
