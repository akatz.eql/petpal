package fr.eql.aicap1.petpal.entity;

public enum PetCategory {

    CHAT,
    CHIEN,
    LAPIN,
    HAMSTER,
    CHINCHILLA,
    OISEAU
}
