package fr.eql.aicap1.petpal.entity;

import java.io.Serializable;
import java.time.LocalDate;

public class Cat extends Pet implements Serializable {

    private Long catId;
    private final CatBreed breed;

    public Cat(Long catId, CatBreed breed, String name, LocalDate birthdate, String picture) {
        super(name, birthdate, picture);
        this.catId = catId;
        this.breed = breed;
    }

    /// Getters ///
    public CatBreed getBreed() {
        return breed;
    }

    /// Setters ///
    public void setCatId(Long catId) {
        this.catId = catId;
    }
}
