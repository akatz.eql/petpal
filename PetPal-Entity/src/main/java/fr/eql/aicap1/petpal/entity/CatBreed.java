package fr.eql.aicap1.petpal.entity;

public enum CatBreed {

    BENGAL,
    NORVEGIEN,
    PERSAN,
    SIAMOIS,
    MUNCHKIN
}
